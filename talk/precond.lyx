#LyX 2.1 created this file. For more info see http://www.lyx.org/
\lyxformat 474
\begin_document
\begin_header
\textclass foils
\begin_preamble
\usepackage{alltt}
\usepackage{pifont}
\usepackage{psfrag}
%*------------------------------------------------------------------------*%
\newif\ifdynamic%
%*------------------------------------------------------------------------*%
\newif\ifpdf%
\ifx\pdfoutput\undefined%
 \dynamicfalse%
\else%
 \dynamictrue%
\fi%
%*------------------------------------------------------------------------*%
\ifpdf
  \usepackage[pdftex]{hyperref}
%  \usepackage[pdftex]{color}
\else
% \usepackage[dvips]{color}
  \def\href#1#2{#2}
\fi
%*------------------------------------------------------------------------*%
\usepackage[table]{xcolor}
\definecolor{c1}{rgb}{0.8,0.85,1}
\definecolor{c2}{rgb}{1,0.9,0.75}
\def\WHITE{white}
\def\YELLOW{yellow}
\ifdynamic
\def\BLUE{blue}
\def\RED{red}
\def\GREEN{green}
\def\MAGENTA{magenta}
\else
\def\BLUE{black}
\def\RED{black}
\def\GREEN{black}
\def\MAGENTA{black}
\fi
\def\BOXBG{\YELLOW}
\def\BOXFG{black}
\def\TITLE{blue}

\definecolor{darkgray}{gray}{.25}

\def\@foilhead[#1]#2{\vfill\eject
  \ifnew@rot@state
    \cur@rot@statetrue\@rotdimens
  \else
    \cur@rot@statefalse\@defaultdimens
  \fi
  {\color@begingroup\normalcolor
   \reset@font\large\bfseries\centering\color{\TITLE}{#2}\par\null\color@endgroup}%
  \advance\foilheadskip by #1 \vspace{\foilheadskip}
  \advance\foilheadskip by -#1}

\usepackage{pstricks}
\usepackage{rotating}
\usepackage{multirow, multicol}
\usepackage{hhline}
\definecolor{darkgreen}{rgb}{0,.67,0}
\definecolor{bulletblue}{rgb}{0,0,0.67}
\end_preamble
\options dvips
\use_default_options false
\maintain_unincluded_children false
\language english
\language_package default
\inputencoding default
\fontencoding global
\font_roman times
\font_sans helvet
\font_typewriter courier
\font_math auto
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100
\font_tt_scale 100
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize 25
\spacing single
\use_hyperref false
\papersize letterpaper
\use_geometry true
\use_package amsmath 1
\use_package amssymb 1
\use_package cancel 1
\use_package esint 0
\use_package mathdots 0
\use_package mathtools 1
\use_package mhchem 1
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine basic
\cite_engine_type default
\biblio_style plain
\use_bibtopic false
\use_indices false
\paperorientation landscape
\suppress_date false
\justification true
\use_refstyle 0
\index Index
\shortcut idx
\color #008000
\end_index
\leftmargin 1cm
\topmargin 1.5cm
\rightmargin 1cm
\bottommargin 2cm
\secnumdepth 1
\tocdepth 1
\paragraph_separation indent
\paragraph_indentation default
\quotes_language polish
\papercolumns 1
\papersides 1
\paperpagestyle default
\bulletLaTeX 0 "\color{bulletblue}{\ding{117}}"
\bullet 3 0 9 -1
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
MyLogo{Cyrille Artho, 2016-03-15}
\end_layout

\begin_layout Plain Layout

\end_layout

\begin_layout Plain Layout


\backslash
newcommand{
\backslash
cframebox}[1]{
\backslash
fcolorbox{
\backslash
BOXFG}{
\backslash
BOXBG}{#1}}
\end_layout

\end_inset


\end_layout

\begin_layout Title
Precondition Coverage in Software Testing
\end_layout

\begin_layout Author

\series bold
Cyrille Artho
\series default

\begin_inset Newline newline
\end_inset

Information Technology Research Institute (ITRI)
\begin_inset Newline newline
\end_inset

Nat.
\begin_inset space \space{}
\end_inset

Inst.
\begin_inset space \space{}
\end_inset

of Advanced Industrial Science and Technology (AIST),
\begin_inset Newline newline
\end_inset

Ikeda, Osaka, Japan
\family typewriter
\series bold

\begin_inset Newline newline
\end_inset

c.artho@aist.go.jp
\begin_inset Newline newline
\end_inset


\begin_inset space ~
\end_inset


\begin_inset Newline newline
\end_inset


\family default
\series default
Quentin Gros, Guillaume Rousset
\begin_inset Newline newline
\end_inset

University of Nantes, France
\end_layout

\begin_layout Date
2016-03-15
\end_layout

\begin_layout Foilhead
Design by contract
\end_layout

\begin_layout Standard
\align center
\begin_inset Graphics
	filename pics/design-by-contract.fig
	scale 150

\end_inset


\end_layout

\begin_layout Standard
\noindent

\series bold
\color red
Eiffel:
\end_layout

\begin_layout Itemize
Preconditions/postconditions as language features.
\end_layout

\begin_layout Itemize
3318 preconditions in base libraries.
\end_layout

\begin_layout Foilhead
Outline
\end_layout

\begin_layout Standard
\align center
\begin_inset Box Frameless
position "t"
hor_pos "c"
has_inner_box 1
inner_pos "t"
use_parbox 0
use_makebox 0
width "50col%"
special "none"
height "1in"
height_special "totalheight"
status open

\begin_layout Enumerate
Preconditions in libraries
\end_layout

\begin_layout Enumerate
Experiments: Scala library
\end_layout

\begin_layout Enumerate
Preconditions in test models
\end_layout

\begin_layout Enumerate
Experiments: Modbat models
\end_layout

\begin_layout Enumerate
Conclusion
\end_layout

\end_inset


\end_layout

\begin_layout ShortFoilhead
Scala
\end_layout

\begin_layout Itemize
A 
\series bold
\color red
functional
\color inherit
 and 
\color blue
object-oriented
\series default
\color inherit
 programming language.
\end_layout

\begin_layout Itemize
Compiles to Java bytecode.
\end_layout

\begin_deeper
\begin_layout Itemize
Runs on the Java VM.
\end_layout

\begin_layout Itemize
Statically typed (more expressive type system than Java).
\end_layout

\begin_layout Itemize
Re-use of all Java libraries, mix of Java and Scala possible.
\end_layout

\end_deeper
\begin_layout Itemize
Elegant, expressive syntax (type inference).
\begin_inset VSpace -5mm
\end_inset


\end_layout

\begin_layout Standard
\noindent

\family typewriter
\series bold
\color blue
var
\color inherit
 name: String = 
\begin_inset Quotes pld
\end_inset

Hello, world
\begin_inset Quotes prd
\end_inset


\color blue

\begin_inset Newline newline
\end_inset

var
\color inherit
 name = 
\begin_inset Quotes pld
\end_inset

Hello, world
\begin_inset Quotes prd
\end_inset


\family default
\series default

\begin_inset VSpace vfill
\end_inset


\end_layout

\begin_layout Standard
\align center
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
cframebox{
\backslash
textbf{Support for preconditions around version 2.8 (2010)}}
\end_layout

\end_inset


\end_layout

\begin_layout Foilhead
Preconditions in Scala libraries
\end_layout

\begin_layout Standard
\align center

\size large
\begin_inset Tabular
<lyxtabular version="3" rows="3" columns="2">
<features rotate="0" tabularvalignment="middle">
<column alignment="center" valignment="top">
<column alignment="left" valignment="top">
<row>
<cell multirow="3" alignment="center" valignment="middle" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\size large
\begin_inset Graphics
	filename pics/3way-choice.fig
	scale 40

\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\family typewriter
\series bold
\size large
require
\family default
\series default
 statements
\end_layout

\end_inset
</cell>
</row>
<row>
<cell multirow="4" alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\size large
IllegalArgumentException
\end_layout

\end_inset
</cell>
</row>
<row>
<cell multirow="4" alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\size large
IllegalStateException
\end_layout

\end_inset
</cell>
</row>
</lyxtabular>

\end_inset


\begin_inset VSpace vfill
\end_inset


\end_layout

\begin_layout Itemize

\size large
Only 
\series bold
39 
\series default
preconditions in core libraries!
\begin_inset VSpace vfill
\end_inset


\end_layout

\begin_layout Standard
\align center

\size large
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
cframebox{
\backslash
textbf{How effectively are preconditions tested?}}
\end_layout

\end_inset


\begin_inset VSpace vfill
\end_inset


\end_layout

\begin_layout Foilhead
Tests and preconditions
\end_layout

\begin_layout Description

\size large
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
color{darkgreen}
\end_layout

\end_inset

Pass:
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
normalcolor
\end_layout

\end_inset

 Correct use of library covered.
\begin_inset Newline newline
\end_inset


\begin_inset space ~
\end_inset


\end_layout

\begin_layout Description

\size large
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
color{red}
\end_layout

\end_inset

Fail:
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
normalcolor
\end_layout

\end_inset

 Incorrect use exposed.
\begin_inset Newline newline
\end_inset


\end_layout

\begin_layout Foilhead
Tests and preconditions
\end_layout

\begin_layout Description

\size large
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
color{darkgreen}
\end_layout

\end_inset

Pass:
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
normalcolor
\end_layout

\end_inset

 Correct use of library covered.
\begin_inset Newline newline
\end_inset


\emph on
Function can be used
\series bold
 
\series default
\emph default
(as intended).
\end_layout

\begin_layout Description

\size large
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
color{red}
\end_layout

\end_inset

Fail:
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
normalcolor
\end_layout

\end_inset

 Incorrect use exposed.
\begin_inset Newline newline
\end_inset


\emph on
Function catches at least some faulty usages.
\end_layout

\begin_layout Foilhead
Lack of coverage
\end_layout

\begin_layout Description

\size large
No
\begin_inset space ~
\end_inset


\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
color{darkgreen}
\end_layout

\end_inset

pass:
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
normalcolor
\end_layout

\end_inset

 Not sure if function is usable!
\begin_inset Newline newline
\end_inset


\begin_inset space ~
\end_inset


\end_layout

\begin_layout Description

\size large
No
\begin_inset space ~
\end_inset


\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
color{red}
\end_layout

\end_inset

fail:
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
normalcolor
\end_layout

\end_inset

 Not sure if precondition is effective.
\begin_inset Newline newline
\end_inset


\begin_inset space ~
\end_inset


\begin_inset VSpace vfill
\end_inset


\end_layout

\begin_layout Standard
\align center

\size large
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
cframebox{
\backslash
textbf{Precondition wrong or test suite bad?}}
\end_layout

\end_inset


\begin_inset VSpace vfill
\end_inset


\end_layout

\begin_layout Foilhead
Precondition coverage in the Scala library
\end_layout

\begin_layout Standard
\align center
\begin_inset Tabular
<lyxtabular version="3" rows="3" columns="5">
<features rotate="0" tabularvalignment="middle">
<column alignment="right" valignment="top">
<column alignment="center" valignment="top">
<column alignment="center" valignment="top">
<column alignment="center" valignment="top">
<column alignment="center" valignment="top">
<row>
<cell multicolumn="1" alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\size large
Unit tests
\end_layout

\end_inset
</cell>
<cell multicolumn="2" alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell multicolumn="1" alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\size large
All tests
\size default

\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
vspace{5mm}
\end_layout

\end_inset


\end_layout

\end_inset
</cell>
<cell multicolumn="2" alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\size large
Pass
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\size large
Fail
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\size large
Pass
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\size large
Fail
\size default

\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
vspace{5mm}
\end_layout

\end_inset


\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\size large
22/39
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\size large
2/39
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\size large
27/39
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\size large
7/39
\end_layout

\end_inset
</cell>
</row>
</lyxtabular>

\end_inset


\size large

\begin_inset VSpace vfill
\end_inset


\end_layout

\begin_layout Itemize

\size large
10/39 cases are never tested.
\end_layout

\begin_layout Itemize

\size large
Low coverage:
\end_layout

\begin_deeper
\begin_layout Itemize

\size large
More tests needed?
\end_layout

\begin_layout Itemize

\size large
Precondition too strong/weak?
\end_layout

\end_deeper
\begin_layout Foilhead
Model-based testing
\end_layout

\begin_layout Standard
\align center
\begin_inset Graphics
	filename pics/mbt.fig
	scale 110

\end_inset


\size large

\begin_inset VSpace -3mm
\end_inset


\end_layout

\begin_layout Itemize
Model contains:
\size large

\begin_inset VSpace -3mm
\end_inset


\end_layout

\begin_deeper
\begin_layout Itemize
Formalized description of the system behavior.
\end_layout

\begin_layout Itemize

\series bold
\color red
Preconditions
\series default
\color inherit
, action, expected output or state.
\end_layout

\end_deeper
\begin_layout Itemize
Transformation tool generates (and/or executes) test cases.
\end_layout

\begin_layout Foilhead
Preconditions in test models
\end_layout

\begin_layout Standard
\align center
\begin_inset Graphics
	filename pics/precond-in-mbt.fig
	scale 150

\end_inset


\size large

\begin_inset VSpace vfill
\end_inset


\end_layout

\begin_layout Standard
\align center
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
cframebox{
\backslash
textbf{
\backslash
large{Test action is enabled if preconditions hold.}}}
\end_layout

\end_inset


\size large

\begin_inset VSpace vfill
\end_inset


\end_layout

\begin_layout Foilhead
Modbat models
\end_layout

\begin_layout Standard
\begin_inset Tabular
<lyxtabular version="3" rows="2" columns="2">
<features rotate="0" tabularvalignment="middle">
<column alignment="center" valignment="top">
<column alignment="center" valignment="top">
<row>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\series bold
Model code
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\series bold
Workflow
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
hspace{-1.2cm}
\end_layout

\end_inset


\begin_inset Box Frameless
position "t"
hor_pos "c"
has_inner_box 1
inner_pos "t"
use_parbox 0
use_makebox 0
width "68col%"
special "none"
height "1in"
height_special "totalheight"
status open

\begin_layout LyX-Code

\series bold
\size small
def next {
\end_layout

\begin_layout LyX-Code

\series bold
\size small
\color red
  require (valid)
\end_layout

\begin_layout LyX-Code

\series bold
\size small
\color red
  require (pos < data.n-1)
\end_layout

\begin_layout LyX-Code

\series bold
\size small
  val res = SUT.next
\end_layout

\begin_layout LyX-Code

\series bold
\size small
  
\color blue
pos += 1
\end_layout

\begin_layout LyX-Code

\series bold
\size small
  
\begin_inset ERT
status open

\begin_layout Plain Layout

{
\backslash
color{darkgreen}assert (data.array(pos) == res)}
\end_layout

\end_inset


\end_layout

\begin_layout LyX-Code

\series bold
\size small
}
\end_layout

\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Box Frameless
position "t"
hor_pos "c"
has_inner_box 1
inner_pos "t"
use_parbox 0
use_makebox 0
width "33col%"
special "none"
height "1in"
height_special "totalheight"
status open

\begin_layout Plain Layout

\series bold
\size small
\begin_inset Phantom Phantom
status open

\begin_layout Plain Layout

\series bold
\size small
(function definition)
\end_layout

\end_inset


\end_layout

\begin_layout Plain Layout

\series bold
\size small
\color red
1.
\begin_inset space ~
\end_inset

precondition 1
\end_layout

\begin_layout Plain Layout

\series bold
\size small
\color red
\begin_inset Phantom HPhantom
status open

\begin_layout Plain Layout

\series bold
\size small
\color red
1.
\end_layout

\end_inset


\begin_inset space ~
\end_inset

precondition 2
\end_layout

\begin_layout Plain Layout

\series bold
\size small
2.
\color red

\begin_inset space ~
\end_inset


\color inherit
action on SUT
\end_layout

\begin_layout Plain Layout

\series bold
\size small
\color blue
\begin_inset Phantom HPhantom
status open

\begin_layout Plain Layout

\series bold
\size small
\color blue
2.
\end_layout

\end_inset


\color red

\begin_inset space ~
\end_inset


\color blue
update model var.
\end_layout

\begin_layout Plain Layout

\series bold
\size small
\begin_inset ERT
status open

\begin_layout Plain Layout

{
\backslash
color{darkgreen}3.~verify result}
\end_layout

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset space ~
\end_inset


\end_layout

\end_inset


\end_layout

\end_inset
</cell>
</row>
</lyxtabular>

\end_inset


\end_layout

\begin_layout Standard
\align center

\size large
\begin_inset VSpace vfill
\end_inset


\size default

\begin_inset Tabular
<lyxtabular version="3" rows="3" columns="3">
<features rotate="0" tabularvalignment="middle">
<column alignment="center" valignment="top">
<column alignment="center" valignment="top">
<column alignment="center" valignment="top">
<row>
<cell multicolumn="1" alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\series bold
\color red
Precondition
\end_layout

\end_inset
</cell>
<cell multicolumn="2" alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell multicolumn="2" alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\series bold
Always true
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\series bold
Always false
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Precondition is an invariant
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Test action cannot be executed
\end_layout

\end_inset
</cell>
</row>
</lyxtabular>

\end_inset


\end_layout

\begin_layout ShortFoilhead
Java collections (
\family typewriter
java.util
\family default
)
\end_layout

\begin_layout Standard
\align center
\begin_inset Graphics
	filename ../plot/plot-slide.eps
	scale 190

\end_inset


\end_layout

\begin_layout ShortFoilhead
Java networking (
\family typewriter
java.nio
\family default
)
\end_layout

\begin_layout Standard
\align center
\begin_inset Graphics
	filename ../plot/nio-slide.eps
	scale 190

\end_inset


\end_layout

\begin_layout Foilhead
Summary
\size large

\begin_inset VSpace vfill
\end_inset


\end_layout

\begin_layout Standard
\align center
\begin_inset ERT
status open

\begin_layout Plain Layout

{
\backslash
Large
\end_layout

\end_inset


\begin_inset Tabular
<lyxtabular version="3" rows="3" columns="3">
<features rotate="0" tabularvalignment="middle">
<column alignment="center" valignment="top">
<column alignment="center" valignment="top">
<column alignment="center" valignment="top">
<row>
<cell multicolumn="1" alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\series bold
\color red
Precondition
\end_layout

\end_inset
</cell>
<cell multicolumn="2" alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell multicolumn="2" alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\series bold
Always true
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\series bold
Always false
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Too weak?
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Too strong?
\end_layout

\end_inset
</cell>
</row>
</lyxtabular>

\end_inset


\begin_inset ERT
status open

\begin_layout Plain Layout

}
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\align center

\size large
\begin_inset VSpace vfill
\end_inset


\end_layout

\begin_layout Standard
\align center

\size large
\SpecialChar \ldots{}
 or is the test suite just too weak/small?
\begin_inset VSpace vfill
\end_inset


\end_layout

\begin_layout Foilhead
Future work: Mutually exclusive preconditions
\begin_inset Newline newline
\end_inset

in test models?
\end_layout

\begin_layout Standard

\size large
\begin_inset VSpace vfill
\end_inset


\end_layout

\begin_layout Standard
\align center

\size large
\begin_inset Graphics
	filename dot/precond.eps
	scale 250

\end_inset


\begin_inset VSpace vfill
\end_inset


\end_layout

\end_body
\end_document
